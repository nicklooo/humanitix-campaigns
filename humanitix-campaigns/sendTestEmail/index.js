/**
 * http function to send out users test emails
 */
"use strict";
require("dotenv").config();

function addHeaders(res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Methods', 'POST, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('accept', 'application/json');
    return res;
}

async function sendTestEmail(campaign, to) {
    // wait for the database to connect.
    return true;
}

function optionsCall(context, req, res) {
    res
        .status(204)
        .json({status: "Success"});
}

async function postCall(context, req, res) {
    try {
        const {campaign, to} = req.body;
        await sendTestEmail(campaign, to);
        res
            .status(200)
            .json({status: "Success"});
    } catch (err) {
        context.log(err)
        res
            .status(500)
            .json({status: "Error"});
    }
}

module.exports = function (context, req) {
    let res = addHeaders(context.res);
    switch (req.method) {
        case 'OPTIONS':
            optionsCall(context, req, res);
            break;
        case 'POST':
            postCall(context, req, res);
            break;
    }

}