/**
 * An Azure timer function to send out campaign emails
 */
require("dotenv").config();

async function startScheduled(context) {
    // wait for the database to connect.
    try {
        context.log('Emails sent');
        context.done();
        return;
    } catch (err){
        context.log(err);
        context.done();
        return;
    } 
}

module.exports = function (context, myTimer) {
    startScheduled(context)
};